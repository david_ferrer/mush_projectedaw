<?php

require_once './PHPMailer-master/PHPMailerAutoload.php';

if(checkPostedValues()){
    $name =  $_POST['name'];
    $email = $_POST['mail'];
    $subject = $_POST['subject'];
    $comment = $_POST['comment'];
    
    $mail = new PHPMailer;
    //$mail->SMTPDebug = 3;  // Enable verbose debug output

    $username = 'a14davferpov@iam.cat'; //username
    $password = 'Ilovetatu7/'; //password
    
    $mail->isSMTP();                            // Set mailer to use SMTP
    $mail->Host = 'smtp.gmail.com';             // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                     // Enable SMTP authentication
    $mail->Username = $username;                // SMTP username
    $mail->Password = $password;                // SMTP password
    $mail->SMTPSecure = 'tls';                  // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 587;   
    
    $mail->setFrom($username);
    
    $mail->AddAddress($username);         // Add a recipient

    $mail->isHTML(true);

    $mail->Subject = "Contacte Web - Spanish Guitar Barcelona";
    $mail->Body =
        "Nom: " . $name . "<BR>" .
        "Email: " . $email . "<BR>" .
        "Assumpte: " . $subject . "<BR>" .
        "Missatge: " . $comment;

    //  This is the body in plain text for non-HTML mail clients
    $mail->AltBody =
        "Nom: " . $name . "\n" .
        "Email: " . $email . "\n" .
        "Assumpte: " . $subject . "\n" .
        "Missatge: " . $comment;

    //echo checkPostedValues();
    if(!$mail->Send()){
        echo "Mailer Error: " . $mail->ErrorInfo;
        echo 'false';
    }else{
        echo 'true';
    }
}else{
    echo 'false';
}
    
function checkPostedValues(){
    if (
      $_POST['name'] == "" ||
      $_POST['mail'] == "" ||
      !checkEmail($_POST['mail']) ||
      $_POST['subject'] == "" ||
      $_POST['comment'] == ""
    ){
        return false;
    }else {
        return true;
    }
}

function checkEmail($emailaddress){
    $pattern = '/^(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){255,})(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){65,}@)(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22))(?:\\.(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-+[a-z0-9]+)*\\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-+[a-z0-9]+)*)|(?:\\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\\]))$/iD';
    if (preg_match($pattern, $emailaddress) === 1){
        return true;
    }else{
        return false;
    }
}

?>
