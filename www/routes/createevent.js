var express = require('express');
var router = express.Router();
var bd = require('./bd');
var crypto = require('crypto');
var fs = require('fs');
var path = require('path')

var app = require('../app.js');

var mysql=require('mysql');

router.get('/', function(req, res, next) {
	console.log("GET [/] -> R: createevent");
	
	if (req.session.username){
		res.writeHead(302, {'Location': '/page'});
		res.end();
	} else {
		res.render('proves');
	}
});

router.post('/', function(req, res, next) {
	console.log("GET [/] -> R: login");
	
	if (req.session.username){
			
		errors = "";
		allOK = true;
		
		// ID
		userid = req.session.userid;
		
		// Name
		name = encodeURI(req.body.name);
		
		// Description
		description = encodeURI(req.body.description);
		
		// Startdate
		startdate = req.body.startdate;
		if (startdate === ""){
			errors += "Startdate error: [" + startdate + "] \n";
			startdate = null;
			allOK = false;
		} else {
			startdate = "'" + startdate + "'";
		}
		
		// Enddate
		enddate = req.body.enddate;
		if (enddate === ""){
			errors += "Enddate error: [" + enddate + "] \n";
			enddate = null;
			allOK = false;
		} else {
			enddate = "'" + enddate + "'";
		}
		
		// Createddate
		var date = new Date();
		var createddate = "";
		createddate += date.getFullYear() + "-";
		createddate += (date.getMonth() + 1 ) + "-";
		createddate += date.getDate() + " ";
		createddate += date.getHours() + ":";
		createddate += date.getMinutes() + ":";
		createddate += date.getSeconds();
		
		// Capacity
		capacity = req.body.capacity;
		
		// Category	
		category = req.body.category;
				
		// Address
		address = req.body.address;
		
		// Age
		age = parseInt(req.body.age);
		
		// Image
		var validImagePath = true;
		var filepath = req.files.image.path;
		var fileextension = path.extname(filepath);
		if (fileextension === ""){
			errors += "Fileextension error: [" + fileextension + "] \n";
			fileextension = null;
			allOK = false;
			validImagePath = false;
		}	
		
		// Lat
		lat = req.body.lat;
		
		// Lng
		lng = req.body.lng;
		
		// Price
		price = req.body.price;
		
		// Phone
		phone = req.body.phone;
		if (phone !== "" || phone.length<9){
			errors += "Phone error: [" + phone + "] \n";
			phone = 987789987;
			allOK = false;
		}
		
		// Web
		web = encodeURI(req.body.web);
		
		// Email
		email = encodeURI(req.body.email);
		
		// Visbility
		visibility = req.body.visibility;
		
		if (!allOK){
			console.log("* ERRORS");
			console.log(errors);
		}
	
		if (validImagePath){
			
		   
	   	// ID image name will be [SECONDS.EXTENSION]
			// Path to know where save image
   		var imageFolder = '/public/images/event_images/';
   		var imageFolderToWrite = "public/images/event_images/"
   		var imageFolderToImgTag = '/images/event_images/';
	   	var d = new Date();
	   	var uniqueSecondsNumber = d.getTime();
	   	var imageName = uniqueSecondsNumber + fileextension;
		   
	   	// Image Path [IDUSER_USERNAME_TIME+EXTENSION]
	   	var imagePath = imageName;
	   	var imagePathToImgTag = "http://" + app.ip + ":" + app.port + imageFolderToImgTag + imageName;
		
	   	var is = fs.createReadStream(filepath);
	   	var os = fs.createWriteStream(imageFolderToWrite + imagePath);
		
	   	is.pipe(os)
		
	   	is.on('end', function() {
	      	//eliminamos el archivo temporal
      		fs.unlinkSync(filepath)
   		})
	   } else {
   		imageName = null;
   	}
   	
   			
		console.log("Id user: [" + userid + "] type: ["+typeof userid+"]");
		console.log("name: [" + name + "] type: ["+typeof name+"]");
		console.log("description: [" + description + "] type: ["+typeof description+"]");
		console.log("startdate: [" + startdate + "] type: ["+typeof startdate+"]");
		console.log("enddate: [" + enddate + "] type: ["+typeof enddate+"]");
		console.log("createddate: [" + createddate + "] type: ["+typeof createddate+"]");
		console.log("capacity: [" + capacity + "] type: ["+typeof capacity+"]");
		console.log("category: [" + category + "] type: ["+typeof category+"]");
		console.log("address: [" + address + "] type: ["+typeof address+"]");
		console.log("age: [" + age + "] type: ["+typeof age+"]");
		console.log("filePath: [" + filepath + "] type: ["+typeof filepath+"]");
		console.log("FileExtension: [" + fileextension + "] type: ["+typeof fileextension+"]");
		console.log("ImageName: [" + imageName + "] type: ["+typeof imageName+"]");
		console.log("lat: [" + lat + "] type: ["+typeof lat+"]");
		console.log("lng: [" + lng + "] type: ["+typeof lng+"]");
		console.log("price: [" + price + "] type: ["+typeof price+"]");
		console.log("phone: [" + phone + "] type: ["+typeof phone+"]");
		console.log("web: [" + web + "] type: ["+typeof web+"]");
		console.log("email: [" + email + "] type: ["+typeof email+"]");
		console.log("visibility: [" + visibility + "] type: ["+typeof visibility+"]");
		
	   
	   // INSERT NEW EVENT QUERY
		// If can insert
		// SQL INJECTION PREVENT
		var sql = "INSERT INTO ?? (??, ??, ??, ??, ??, ??, ??, ??, ??, ??, ??, ??, ??, ??, ??, ??, ??, ??) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		var inserts = ['events', 'iduser', 'name', 'description', 'startdate', 'enddate', 'capacity', 'lat', 'lng', 'category', 'address', 'age', 'imagename', 'price', 'phone', 'web', 'email', 'visibility',
										  userid, name, description, startdate, enddate, capacity, lat, lng, category, address, age, imageName, price, phone, web, email, visibility];
		
		//sql = mysql.format(sql, inserts);

		// La sentència SQL que utilitzarem no serà [sql] sinó [SQLfinal] 
		// perquè sempre em salta errors quan utilitzo el mètode mysql.format() 
		// per controlar les injeccions en la consulta.
		// Potser té a veure amb els símbols '/' de les rutes o que codifico tot amb encodeURI()
		// i dóna error. Com no sé com solucionar-ho ho deixo estar.
		// Llençem contra la BD la consulta tal cual.
		
		SQLfinal = "";
		SQLfinal += "INSERT INTO events VALUES (";
		SQLfinal += null 			+ ", ";
		SQLfinal += userid 		+ ", \"";
		SQLfinal += name 			+ "\", \"";
		SQLfinal += description + "\", ";
		SQLfinal += startdate 	+ ", ";
		SQLfinal += enddate 		+ ", \"";
		SQLfinal += createddate + "\", ";
		SQLfinal += capacity 	+ ", \"";
		SQLfinal += lat			+ "\", \"";
		SQLfinal += lng			+ "\", \"";
		SQLfinal += category		+ "\", \"";
		SQLfinal += address		+ "\", \"";
		SQLfinal += age			+ "\", \"";
		SQLfinal += imageName	+ "\", ";
		SQLfinal += price			+ ", ";
		SQLfinal += phone			+ ", \"";
		SQLfinal += web			+ "\", \"";
		SQLfinal += email			+ "\", \"";
		SQLfinal += visibility	+ "\")";
		
		console.log("SQL: " + SQLfinal);
		
		
		/* SQL INSERT */
		bd.query(SQLfinal,
			[],
			function(error,filas){
				if (!error) {
					console.log('Create Event SUCCESS _console');
														
						res.render('createeventsuccess', {
							name: 			decodeURI(name),
							description:	decodeURI(description),
							startdate:		startdate,
							enddate:			enddate,
							capacity:		capacity,
							category:		category,
							address:			decodeURI(address),
							lat:				lat,
							lng:				lng,
							image_path:		imagePathToImgTag,
							price:			price,
							phone:			phone,
							web:				decodeURI(web),
							email:			(email),
							visibility:		visibility
						});
				} else {
					console.log('Create Event ERROR _console');
					
					res.writeHead(302, {'Location': '/page'});
					res.end();
				}	/* end of INSERT SQL*/
			}
		);
		 	
	} else {
		res.render("index");
	}
})

module.exports = router;