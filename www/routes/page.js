var express = require('express');
var router = express.Router();
var bd = require('./bd');
var crypto = require('crypto');

var mysql=require('mysql');

router.get('/', function(req, res, next) {
	console.log("GET [/] -> R: page");
	
	if (req.session.username){
		
		var eventsArrayObj = [];
		
		var sql = "SELECT * FROM ?? WHERE iduser = ? ORDER BY ?? DESC";
		var inserts = ['events', req.session.userid, 'createddate'];
		sql = mysql.format(sql, inserts);
		
		console.log("SQL: " + sql);
		
		bd.query(sql,
			[],
			function(error,filas,fields){
				if (error) {
					console.log('error en la consulta');
				} else {
					var aux = 0;
					filas.forEach(function () {
						
						/*
						console.log("["+fields[0].name+"-"+filas[aux].id+"]");
						console.log("["+fields[1].name+"-"+filas[aux].iduser+"]");
						console.log("["+fields[2].name+"-"+filas[aux].name+"]");
						console.log("["+fields[3].name+"-"+filas[aux].description+"]");
						console.log("["+fields[4].name+"-"+filas[aux].date+"]");
						console.log("["+fields[5].name+"-"+filas[aux].duration+"]");
						console.log("["+fields[6].name+"-"+filas[aux].capacity+"]");
						console.log("["+fields[7].name+"-"+filas[aux].lat+"]");
						console.log("["+fields[8].name+"-"+filas[aux].lon+"]");
						console.log("["+fields[9].name+"-"+filas[aux].category+"]");
						console.log("["+fields[10].name+"-"+filas[aux].address+"]");
						console.log("["+fields[11].name+"-"+filas[aux].path_image+"]");
						console.log("["+fields[12].name+"-"+filas[aux].price+"]");
						console.log("["+fields[13].name+"-"+filas[aux].schedule+"]");
						console.log("["+fields[14].name+"-"+filas[aux].phone+"]");
						console.log("["+fields[15].name+"-"+filas[aux].web+"]");
						console.log("["+fields[16].name+"-"+filas[aux].email+"]");
						console.log("["+fields[17].name+"-"+filas[aux].visibility+"]");
						*/						
						
						eventsArrayObj.push({
							id:filas[aux].id,
							iduser:filas[aux].iduser,
							name:decodeURI(filas[aux].name),
							description:decodeURI(filas[aux].description),
							startdate:filas[aux].startdate,
							enddate:filas[aux].enddate,
							createddate:filas[aux].createddate,
							capacity:filas[aux].capacity,
							lat:filas[aux].lat,
							lng:filas[aux].lng,
							category:filas[aux].category,
							address:filas[aux].address,
							age:filas[aux].age,
							imagename:filas[aux].imagename,
							price:filas[aux].price,
							phone:filas[aux].phone,
							web:decodeURI(filas[aux].web),
							email:decodeURI(filas[aux].email),
							visibility:filas[aux].visibility
						});
						
						aux++;
						
					}); /* end of forEach */ 
				}
				
				var eventsLen = eventsArrayObj.length;
				console.log("Array length: " + eventsLen);
			
				if (eventsArrayObj.length==0) {
					console.log("No existeix cap event en la BD");
						
					res.render('page', {
						title: "Events List",
						eventsList: "No existeix cap event en la BD"
					});
		
				} else {
					console.log("S'han trobat events en la BD");
					
					var eventsList = "";
					
					for(i=0; i<eventsLen; i++){
					
					eventsList+= "<div class='event-item'>";
					
					if (eventsArrayObj[i].imagename == "null"){
						eventsList+= "<div class='no-image'><h6>No-image</h6></div>";
					} else {
						eventsList+= "			<div class='event_image'>";
						eventsList+= "				<img class='materialboxed my-image-style-eventslist' src='images/event_images/" + eventsArrayObj[i].imagename  +"'></img>";
						eventsList+= "			</div>";
					}
					eventsList+= "			<div class='event_info teal lighten-1 my-small-padding'>";
					eventsList+= '				<ul class="collapsible popout" data-collapsible="accordion">';
					
												// General Info Collapsiable
					eventsList+= '				<li>';
					eventsList+= '					<div class="collapsible-header">';
					eventsList+= '						<i class="material-icons">subject</i>General info';
					eventsList+= '					</div>';
					
					eventsList+= '					<div class="collapsible-body white my-small-padding">';
					
													// Name
					eventsList+= '					<div class="input-field">';
					eventsList+= '						<i id="icon_name" class="material-icons prefix">label</i>';
					eventsList+= '						<input id="show_name" type="text" readonly="true" value="' + eventsArrayObj[i].name + '\">';
					eventsList+= '						<label id="label_name" for="show_name">Name</label>';
					eventsList+= '					</div>';
				
													// Description
					eventsList+= '					<div class="input-field">';
					eventsList+= '						<i class="material-icons prefix">mode_edit</i>';
					eventsList+= '						<textarea id="show_description" name="description" readonly="true" class="materialize-textarea">' + eventsArrayObj[i].description + '</textarea>';
					eventsList+= '						<label for="show_description">Description</label>';
					eventsList+= '					</div>';

													// Dates
					eventsList+= '					<div class="input-field">';
					eventsList+= '						<div class="container">';
					eventsList+= '							<div class="row">';
					eventsList+= '								<div class="col s12">';

					eventsList+= '									<!-- startDate -->';
					eventsList+= '									<div class="col s6 m6">';
					eventsList+= '										<div class="input-field">';
					eventsList+= '											<i id="icon_startdate" class="material-icons prefix">today</i>';
					eventsList+= '											<input id="show_startdate" name="startdate" type="text" readonly="true" value="' + eventsArrayObj[i].startdate + '\">';
					eventsList+= '											<label for="show_startdate">Start date</label>';
					eventsList+= '										</div>';
					eventsList+= '									</div>';
					
					eventsList+= '									<!-- endDate -->';
					eventsList+= '									<div class="col s6 m6">';
					eventsList+= '										<div class="input-field">';
					eventsList+= '											<i id="icon_enddate" class="material-icons prefix">today</i>';
					eventsList+= '											<input id="show_enddate" name="enddate" type="text" readonly="true" value="' + eventsArrayObj[i].enddate + '\">';
					eventsList+= '											<label for="show_enddate">End date</label>';
					eventsList+= '										</div>';
					eventsList+= '									</div>';
											
					eventsList+= '								</div> <!-- end col s12 -->';	
					eventsList+= '							</div> <!-- end row -->';
					eventsList+= '						</div> <!-- end conatiner -->';
					eventsList+= '					</div> <!-- end input-field -->';
								
													// Capacity
					eventsList+= '					<div class="input-field">';
					eventsList+= '						<div class="range-field">';
					eventsList+= '							<i id="icon_capacity" class="material-icons prefix">assignment_ind</i>';
					eventsList+= '							<input id="show_capacity" name="capacity" readonly="true" type="range" min="0" max="1000" value="' + eventsArrayObj[i].capacity + '\">';
					eventsList+= '							<label class="capacity-label" for="show_capacity">Max capacity [' + eventsArrayObj[i].capacity + ']</label>';
					eventsList+= '						</div>';
					eventsList+= '					</div>';
								
													// Price
					eventsList+= '					<div class="input-field">';
					eventsList+= '						<div class="range-field">';
					eventsList+= '							<i id="icon_price" class="material-icons prefix">payment</i>';
					eventsList+= '							<input type="range" name="price" id="show_price" readonly="true" min="0" max="1000" value="' + eventsArrayObj[i].price + '\">';
					eventsList+= '							<label class="price-label" for="show_price">Price [' + eventsArrayObj[i].price + '€]</label>';
					eventsList+= '						</div>';
					eventsList+= '					</div>';
								
													// Category
					eventsList+= '					<div class="input-field">';
					eventsList+= '						<i id="icon_category" class="material-icons prefix my-left">label_outline</i>';
					eventsList+= '						<input id="show_category" name="category" readonly="true" type="text" value="' + eventsArrayObj[i].category + '\">';
					eventsList+= '						<label for="show_category">Category</label>';
					eventsList+= '					</div>';
								
													// Visibility
					eventsList+= '					<div class="input-field">';
					eventsList+= '						<i id="icon_visibility" class="material-icons prefix my-left">visibility</i>';
					eventsList+= '						<input id="show_visibility" name="visibility" readonly="true" type="text" value="' + eventsArrayObj[i].visibility + '\">';
					eventsList+= '					<label for="show_visibility">Visibility</label>';
					eventsList+= '				</div>';
					
													// Age
					eventsList+= '					<div class="input-field">';
					eventsList+= '						<i id="icon-age" class="material-icons fa fa-child prefix my-age-icon" aria-hidden="true"></i>';
					eventsList+= '						<input id="show_age" name="age" type="number" readonly="true" value="' + eventsArrayObj[i].age + '\">';
					eventsList+= '					<label for="show_visibility">Age recommened</label>';
					eventsList+= '				</div>';
					
					
								
					eventsList+= '			</div>';
					eventsList+= '		</li> <!-- end General Info Collapsiable -->';
					
					
					
													// Location Collapsiable
					if ( eventsArrayObj[i].address != "" ){
						eventsList+= '		<li>';
						eventsList+= '			<div class="collapsible-header">';
						eventsList+= '				<i class="material-icons">location_on</i>Location';
						eventsList+= '			</div>';
						eventsList+= '			<div class="collapsible-body no-padding">';
						eventsList+= '				<iframe class="map-event-dynamic my-no-border" src="http://maps.google.com/maps?q=' + eventsArrayObj[i].lat + ',' + eventsArrayObj[i].lng + '&z=15&output=embed"></iframe>';
						eventsList+= '			</div>';
						eventsList+= '		</li>';
					} else {
						eventsList+= '		<li>';
						eventsList+= '			<div class="collapsible-header">';
						eventsList+= '				<i class="material-icons my-color-grey">location_off</i>Location';
						eventsList+= '			</div>';
						eventsList+= '		</li>';
					}

				/*
      				Contact Info Collapiable 
						<li>
							<div class="collapsible-header">
								<i class="material-icons">contact_phone</i>Contact info
							</div>
							<div class="collapsible-body">
				
								<!-- Phone -->
								<div class="input-field">
									<i id="icon_phone" class="material-icons prefix">phone</i>
									<input id="input_phone" name="phone" type="tel" class="validate" required tabindex="10">
									<label for="input_phone">Phone</label>
								</div>
								
								<!-- Web -->
								<div class="input-field">
									<i id="icon_web" class="material-icons prefix">web</i>
									<input id="input_web" name="web" type="text" class="validate" required tabindex="11">
									<label for="input_web">Web</label>
								</div>
								
								<!-- Email -->
								<div class="input-field">
									<i id="icon_email" class="material-icons prefix">email</i>
									<input id="input_email" name="email" type="email" class="validate" required tabindex="12">
									<label for="input_email">Email</label>
								</div>				
				
							</div>
						</li>
*/ 
						
					eventsList+= '					</ul>';
					
													// Name
					eventsList+= "					<span class='eName'>" + eventsArrayObj[i].name + "</span>";
					
													// Description 
					eventsList+= "					<div class='aligned'>";
					eventsList+= "						<i class='material-icons'>receipt</i>";
					eventsList+= "						<span class='eDescription'>" + eventsArrayObj[i].description + "</span>";
					eventsList+= "					</div>";

													// Startdate
					eventsList+= "					<div class='aligned'>";
					eventsList+= "						<i class='material-icons'>today</i>";
					eventsList+= "						<span class='eStartdate'>" + eventsArrayObj[i].startdate + "</span>";
					eventsList+= "					</div>";
					
													// Enddate
					eventsList+= "					<div class='aligned'>";
					eventsList+= "						<i class='material-icons'>today</i>";
					eventsList+= "						<span class='eEnddate'>" + eventsArrayObj[i].enddate + "</span>";
					eventsList+= "					</div>";
					
													// Createddate
					eventsList+= "					<div class='aligned'>";
					eventsList+= "						<i class='material-icons'>today</i>";
					eventsList+= "						<span class='ecreateddate'>" + eventsArrayObj[i].createddate + "</span>";
					eventsList+= "					</div>";
					
													// Category
					eventsList+= "					<div class='aligned'>";
					eventsList+= "						<i class='material-icons'>label</i>";
					eventsList+= "						<span class='eCategory'>" + eventsArrayObj[i].category + "</span>";
					eventsList+= "					</div>";
					
													// Address
					eventsList+= "					<div class='aligned'>";
					eventsList+= "						<i class='material-icons'>room</i>";
					eventsList+= "						<span class='eAddress'>" + eventsArrayObj[i].address + "</span>";
					eventsList+= "					</div>";
					
													// Age
					eventsList+= "					<div class='aligned'>";
					eventsList+= "						<i id='icon-age' class='material-icons fa fa-child prefix my-age-icon' aria-hidden='true'></i>";
					eventsList+= "						<span class='eAge'>" + eventsArrayObj[i].age + "</span>";
					eventsList+= "					</div>";
					
													// Price
					eventsList+= "					<div class='aligned'>";
					eventsList+= "						<i class='material-icons'>payment</i>";
					eventsList+= "						<span class='ePrice'>" + eventsArrayObj[i].price + "</span>";
					eventsList+= "					</div>";
					
													// Phone
					eventsList+= "					<div class='aligned'>";
					eventsList+= "						<i class='material-icons'>phone</i>";
					eventsList+= "						<span class='ePhone'>" + eventsArrayObj[i].phone + "</span>";
					eventsList+= "					</div>";

													// Web
					eventsList+= "					<div class='aligned'>";
					eventsList+= "						<i class='material-icons'>web</i>";
					eventsList+= "						<span class='eWeb'>" + eventsArrayObj[i].web + "</span>";
					eventsList+= "					</div>";
					
													// Email
					eventsList+= "					<div class='aligned'>";
					eventsList+= "						<i class='material-icons'>email</i>";
					eventsList+= "						<span class='eEmail'>" + eventsArrayObj[i].email + "</span>";
					eventsList+= "					</div>";
					
													// Visibility
					eventsList+= "					<div class='aligned'>";
					eventsList+= "						<i class='material-icons'>visibility</i>";
					eventsList+= "						<span class='eVisibility'>" + eventsArrayObj[i].visibility + "</span>";
					eventsList+= "					</div>";
					
					eventsList+= "				</div>"; // end second block (right)
					eventsList+= "			</div>";
										
					} /* end events for */
					
					
		 			res.render('page', {
						title: "Events List",
						username: req.session.username,
						useremail: req.session.useremail,
						eventsList: eventsList,
						userimage: req.session.userimagename,
						userimagebg: req.session.userimagebg
					});
				}
			});
			
		} else {
			res.render('index');
		}
		
});

module.exports = router;