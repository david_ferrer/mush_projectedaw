var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var fs=require('fs');
var multipart = require('connect-multiparty');
var os = require('os');
var ifaces = os.networkInterfaces();

var index = require('./routes/index');
var login = require('./routes/login');
var page = require('./routes/page');
var createaccount = require('./routes/createaccount');
var createevent = require('./routes/createevent');
var logout = require('./routes/logout');


var app = express();

// PORT
var port = 9999;
var listener = app.listen(port, function(){
    console.log('Listening on port ' + listener.address().port); //Listening on port 8888
});

// IP
// Tant la IP com el PORT l'exportem per a utilitzar-ho en altres mòduls
var ip;
Object.keys(ifaces).forEach(function (ifname) {
	var alias = 0;
	
	ifaces[ifname].forEach(function (iface) {
		if ('IPv4' !== iface.family || iface.internal !== false) {
			// skip over internal (i.e. 127.0.0.1) and non-ipv4 addresses
			return;
		}
				
		if (alias >= 1) {
			// this single interface has multiple ipv4 addresses
			console.log("this single interface has multiple ipv4 addresses: " + ifname + ':' + alias, iface.address);
		} else {
			// this interface has only one ipv4 adress
			console.log("this interface has only one ipv4 adress: " + ifname, iface.address);
			ip = iface.address;
		}
	++alias;
	});
});


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({secret: '123456', resave: true, saveUninitialized: true}));
app.use(multipart()) //Express 4

//		Peticions
app.use('/', index);

app.use('/login', login);
app.post('/login', login);

app.use('/page', page);

app.use('/createaccount', createaccount);
app.post('/createaccount', createaccount);

app.use('/createevent', createevent);
app.post('/createevent', createevent);

app.use('/logout', logout);

// FAV ICON
app.use(favicon(path.join(__dirname, 'public', 'mush.ico')))


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  res.render('error', {errorMessage: "This page is not available."});
});

// error handlers

// development error handler
// will print stacktrace
/*
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}
*/

// production error handler
// no stacktraces leaked to user
/*
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});
*/


module.exports.port = port;
module.exports.ip = ip;
module.exports = app;
