// GLOBALS

$(function () {
	console.log("* google-maps.js carregat");
	
	$("#getLocation").on('click', getLocation);
	$("#resetLocation").on('click', resetLocation);
	
});

function initAllMaps() {
	console.log("INIT MAPS");

	newEventDivMapObj = $('#map-new-event');	
	console.log("New event maps len...: " + newEventDivMapObj.length);
	
	if (newEventDivMapObj.length)
		initMapsNewEvent();
	
}

function initializeMapEvent(mapId){
	console.log("Init map with id: " + mapId);
	
	$map = $("#map-event-"+mapId);
	lat = parseFloat( $map.find(".lat").val() );
	lng = parseFloat( $map.find(".lng").val() );
	
	console.log("lat: " + lat);
	console.log("lng: " + lng);
	
	// Location
	var loc = {lat: lat, lng: lng}; 
	
	// Map
	var map = new google.maps.Map(document.getElementById('map-event-'+mapId), {
		center: loc,
		zoom: 8,
		mapTypeId: 'roadmap'
	});
	
	// Mark
	var marker = new google.maps.Marker({
		position: loc,
		map: map
	});
}

var mapNewEvent;
var markers = [];
function initMapsNewEvent() {
	console.log(" -> initMapsNewEvent");

		initLocationBarcelona = {lat: 41.3935855, lng: 2.072792}

		var mapNewEvent = new google.maps.Map(document.getElementById('map-new-event'), {
          center: initLocationBarcelona,
          zoom: 13,
          mapTypeId: 'roadmap'
        });

        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        mapNewEvent.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        mapNewEvent.addListener('bounds_changed', function() {
          searchBox.setBounds(mapNewEvent.getBounds());
        });

			markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }

          // Clear out the old markers.
          markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];

          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
          places.forEach(function(place) {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");
              return;
            }
            var icon = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };

            // Create a marker for each place.
            markers.push(new google.maps.Marker({
              map: mapNewEvent,
              icon: icon,
              title: place.name,
              position: place.geometry.location
            }));

            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
          mapNewEvent.fitBounds(bounds);
        });
     
}

function refreshMap(mapId) {
	setTimeout(function () {
		console.log("REFRESH MAP!");
		google.maps.event.trigger(document.getElementById(mapId), 'resize');
	}, 10);
}



function getLocation() {

	console.log("show location");
	console.log("Locs len: " + markers.length);
	
	var lat, lng;
	
	if (markers.length == 0){
		Materialize.toast('Have not location getter', 1000);
		$(".switchColorLocation").addClass("no-location");
	} else if (markers.length == 1){
	
   	for (var i = 0; i < markers.length; i++) {
   		mark = markers[i].getPosition();
			
			lat = mark.lat();
			lng = mark.lng();
			address = $("#pac-input").val();
			
			console.log("MARK pos: ");
			console.log("Lat: " + lat);
			console.log("Lon: " + lng);
						
		}
		
		$("#lat").attr("value", lat);
		$("#lng").attr("value", lng);
		$("#address").attr("value", address);
		
		
		Materialize.toast(address, 1000);
		$(".switchColorLocation").removeClass("my-color-grey");
	} else if (markers.length > 1){
		Materialize.toast('Too many locations', 1000);
		$(".switchColorLocation").addClass("my-color-grey");
	} else {
		Materialize.toast('An error ocurred... please try later', 1000);
		$(".switchColorLocation").addClass("my-color-grey");
	}
}	

function resetLocation() {
	// Clear out the old markers.
	markers.forEach(function(marker) {
		marker.setMap(null);
	});
	
	markers = [];
	
	$("#pac-input").val("");
	$(".switchColorLocation").addClass("my-color-grey");
	
	Materialize.toast('Location cleaned', 1000);
	
	
}

var checkLocationation = function () {
	if (markers.length == 0)
		return false;
		
	return true;
};
