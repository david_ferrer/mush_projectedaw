$(function () {
	console.log("* form-create.js carregat");
	
	$("#icon_name").addClass("active");
	$("#input_name").addClass("active").focus();
	$("#label_name").addClass("active");
	
	$("#submit-create").on('click', checkCreateAccountFormAndSend);

	// si s'apreta la tecla [ENTER] en qualsevol input, el form s'envia
	$("#create-form").find("input").keypress(pressedEnterKeySendCreateForm);
});

function pressedEnterKeySendCreateForm( event ) {
	if ( event.which == 13 ){
		console.log("ENTER");
		event.preventDefault();
		checkCreateAccountFormAndSend();
	}
}

function checkCreateAccountFormAndSend() {
	console.log("check create account form");
	var itsAllOk = true;
	
	// NICKNAME
	inName = $("#input_name").val();
	inSurName = $("#input_surname").val();
	inUserName = $("#input_username").val();
	inPassword = $("#input_password").val();
	inPhone = $("#input_phone").val();
	inEmail = $("#input_email").val();
	
	console.log(inName);
	console.log(inSurName);
	console.log(inUserName);
	console.log(inPassword);
	console.log(inPhone);
	console.log(inEmail);
	
	// Name
	if (inName === ''){
		itsAllOk = false;
		$("#icon_name").css("color", "red");
	} else {
		$("#icon_name").css("color", "#26A69A");
	}
	
	// Surname
	if (inSurName === ''){
		itsAllOk = false;
		$("#icon_surname").css("color", "red");
	} else {
		$("#icon_surname").css("color", "#26A69A");
	}
		
	// UserName
	if (inUserName === ''){
		itsAllOk = false;
		$("#icon_username").css("color", "red");		
	} else {
		$("#icon_username").css("color", "#26A69A");
	}
	
	// Password
	if (inPassword === ''){
		itsAllOk = false;
		$("#icon_password").css("color", "red");
	} else {
		$("#icon_password").css("color", "#26A69A");
	}
	
	// Phone
	if (!validatePhone(inPhone) ){
		itsAllOk = false;
		$("#icon_phone").css("color", "red");
	} else {
		$("#icon_phone").css("color", "#26A69A");
	}
		
	// Email
	if (inEmail === '' || !validateEmail(inEmail) ){
		itsAllOk = false;
		$("#icon_email").css("color", "red");
	} else {
		$("#icon_email").css("color", "#26A69A");
	}
		
	
	if (itsAllOk)
		$("#create-form").submit();
}

function validateEmail(email) {
    var re = /^[a-z][a-zA-Z0-9_.]*(\.[a-zA-Z][a-zA-Z0-9_.]*)?@[a-z][a-zA-Z-0-9]*\.[a-z]+(\.[a-z]+)?$/;
    return re.test(email);
}
function validatePhone(phone) {
	nineNumbersRegExp=/^([0-9]+){9}$/;
	noneWhiteSpacesRegExp=/\s/;
	
	if(
		!nineNumbersRegExp.test(phone) ||
		noneWhiteSpacesRegExp.test(phone)
	)
		return false;
	
	return true;
}

